# README #
This repo contains a traslation of the implementation of unification formalism, found in "Speech and Language Processing" (Jurafsky, Martin, 2nd edition). 
In the book the algorythm is in pseudocode. I've tried to trasnlate it in python and i've used my programming skills to do that, but any modification and 
Evolutive to the algorythm is welcome from anyone, just email me at agnese.camellini@gmail.com to be subscribed to the repo.

### What is this repository for? ###
This repo is to collaborate and grow in natural language processing collaboratively by contributing to an algorythm translated from a manual pseudocode to python.

### How do I get set up? ###
The algorythm is just a function you can use as standalone module for a software or integrate in more complex modules.

### Contribution guidelines ###
Any contribution is welcome and will be written in comments in the code.

### Who do I talk to? ###
I talk to natural language processing students or developers who want to have more experience in natural language processing.

### License ###
The code is published under GLP version 2.